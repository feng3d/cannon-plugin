import { functionwrap, Scene, View } from 'feng3d';
import { PhysicsWorld } from '../components/PhysicsWorld';

// 默认在 Scene.init 添加物理世界模块
functionwrap.extendFunction(View, 'createNewScene', function (scene: Scene)
{
    scene.object3D.addComponent(PhysicsWorld);

    return scene;
});
